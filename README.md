# mars_rover

You can use RVM or just normal ruby installation

$ rvm use ruby 2.4.0 (for rvm users)

$ gem install bundler
$ bundle install

Run the main rover script file for the test output:
  ruby rover_commander_multi_hard_coded.rb
  
Run the rover_commander.rb script with desired inputs(in order as shown below):
  
  ruby rover_commander.rb `boundry_sie_x` `boundry_size_y` `rover_start_pos_x` `rover_start_pos_y` `rover_start_direction`
  
  E.G: ruby rover_commander.rb 5 5 1 2 N LMLMLMLMM

Run the rspec unit tests
  rspec spec/*

The Rspec Unit tests will run a few tests on the movement code for the rover, and finnaly tets the assesment inputs