require_relative('rover.rb')
class RoverManager

  attr_accessor :boundry_size_x, :boundry_size_y, :rovers

  def initialize(boundry_size_x = 5, boundry_size_y = 5, rovers = [])
    @boundry_size_x = boundry_size_x
    @boundry_size_y = boundry_size_y
    @rovers = rovers
  end

  # Start a rover on given co-ordinates and facing a direction
  # Adds then to the RoverManagers rover array
  def start_new_rover(pos_x = 0, pos_y = 0, direction = :N)
    rover = Rover.new(pos_x, pos_y, direction, @boundry_size_x, @boundry_size_y)
    @rovers << rover
    return rover
  end

  # Gets the position for one rover specifired
  def get_position_of rover
    "#{rover.pos_x} #{rover.pos_y} #{rover.direction}"
  end

  # Uses the RoverManages array list of rovers and gets all of the rovers positions
  def get_position_of_all_rovers
    @rovers.map{| rover | p get_position_of rover }
  end

end